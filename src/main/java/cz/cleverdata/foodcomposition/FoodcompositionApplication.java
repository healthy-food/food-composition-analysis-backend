package cz.cleverdata.foodcomposition;

import org.bytedeco.ffmpeg.global.avformat;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;

@SpringBootApplication(scanBasePackages = {"cz.cleverdata"})
public class FoodcompositionApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodcompositionApplication.class, args);
    }

}
