package cz.cleverdata.foodcomposition.ahocorasick;

import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class AhocorasickUtils {

    public static boolean containsWordsAhoCorasick(String inputString, String words) {
        return containsWordsAhoCorasick(inputString, words.split(","));
    }

    public static boolean containsWordsAhoCorasick(String inputString, List<String> words) {
        return containsWordsAhoCorasick(inputString, words.toArray(new String[words.size()]));
    }

    public static boolean containsWordsAhoCorasick(String inputString, String[] words) {
        Trie trie = Trie.builder().onlyWholeWords().addKeywords(words).build();

        Collection<Emit> emits = trie.parseText(inputString);
//        emits.forEach(System.out::println);

        boolean found = true;
        for (String word : words) {
            boolean contains = Arrays.toString(emits.toArray()).contains(word);
            if (!contains) {
                found = false;
                break;
            }
        }

        return found;
    }
}
