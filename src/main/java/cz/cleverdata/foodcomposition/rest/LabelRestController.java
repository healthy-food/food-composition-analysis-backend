package cz.cleverdata.foodcomposition.rest;

import cz.cleverdata.foodcomposition.dto.LabelDto;
import cz.cleverdata.foodcomposition.utils.MatUtils;
import cz.cleverdata.foodcomposition.utils.OpenCVAndImageStableUtils;
import cz.cleverdata.foodcomposition.utils.TesseractUtils;
import cz.cleverdata.foodcomposition.utils.dto.ImageInfoDPI;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.bytedeco.opencv.opencv_core.Mat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/api/uploadLabel/")
public class LabelRestController {

    @Autowired
    private Environment environment;

    @Autowired
    OpenCVAndImageStableUtils openCVAndImageStableUtils;


    static final double MINIMUM_DESKEW_THRESHOLD = 0.05d;

    @GetMapping
    public String test() {
        return "test";
    }


    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, value = "sync")
    public LabelDto handleFileUpload(@RequestParam(value = "file", required = false) MultipartFile file, @RequestAttribute(value = "imageName", required = false) String imageName) throws IOException {
        String text = null;
        File tempFile = null;
        ImageInfoDPI imageInfoDPI = null;
        Boolean isTestProfileActive = environment.acceptsProfiles(Profiles.of("test", "debug"));
        try {
            if (StringUtils.isBlank(imageName)) {
                imageName = RandomStringUtils.randomAlphabetic(5);
            }
            imageName = "target/" + imageName;
            // ask JVM to ask operating system to create temp file
            tempFile = File.createTempFile("temp_pref", ".jpg");

// ask JVM to delete it upon JVM exit if you forgot / can't delete due exception
            tempFile.deleteOnExit();

// transfer MultipartFile to File
            file.transferTo(tempFile);

            BufferedImage bufferedImage = ImageIO.read(tempFile);
//            imageInfoDPI = ImageUtisHelper.getImageInfo(new FileInputStream(tempFile), imageName);
//            if (imageInfoDPI.getPhysicalHeightDpi() < 300 || imageInfoDPI.getPhysicalWidthDpi() < 300) {
//                log.warn("File " + tempFile + " has low resolution heigth=" + imageInfoDPI.getPhysicalHeightDpi() + " width=" + imageInfoDPI.getPhysicalWidthDpi());
//            }
            Mat mat = MatUtils.BufferedImage2Mat(bufferedImage);
//            mat = openCVAndImageStableUtils.colorToGray(mat,file.getName());
//            mat = openCVAndImageStableUtils.gaussianBlurNoiseRemoval(mat,file.getName());
//            mat = openCVAndImageStableUtils.toBlackAndWhiteAdaptive(mat,file.getName());
            text = TesseractUtils.readBytedecoText(mat);
//            text = TesseractUtils.readText(MatUtils.Mat2BufferedImage(mat));

        } catch (Exception e) {
            log.error("Error during processing ", e);
            text = "Error on backend: " + e.getStackTrace();
        } finally {
            // tidy up
            tempFile.delete();
        }

        return LabelDto.builder().text(text).imageInfoDPI(imageInfoDPI).build();
    }


    //    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, value = "async")
//    public Flux<String> uploadLabel(@RequestBody Flux<Part> parts) {
//        return parts
//                .filter(part -> part instanceof FilePart) // only retain file parts
//                .ofType(FilePart.class) // convert the flux to FilePart
//                .flatMap(this::saveFile); // save each file and flatmap it to a flux of results
//    }
//
//    /**
//     * tske a {@link FilePart}, transfer it to disk using {@link AsynchronousFileChannel}s and return a {@link Mono} representing the result
//     *
//     * @param filePart - the request part containing the file to be saved
//     * @return a {@link Mono} representing the result of the operation
//     */
//    private Mono<String> saveFile(FilePart filePart) {
//        log.info("handling file upload {}", filePart.filename());
//
//        // if a file with the same name already exists in a repository, delete and recreate it
//        final String filename = filePart.filename();
//        File tempFile = null;
//        try {
//            tempFile = File.createTempFile("temp_" + filename, ".jpg");
//            // ask JVM to delete it upon JVM exit if you forgot / can't delete due exception
//            tempFile.deleteOnExit();
//        } catch (Exception e) {
//            return Mono.error(e); // if creating a new file fails return an error
//        }
//
//        try {
//            // create an async file channel to store the file on disk
//            final AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(tempFile.toPath(), StandardOpenOption.WRITE);
//
//            final CloseCondition closeCondition = new CloseCondition();
//
//            // pointer to the end of file offset
//            AtomicInteger fileWriteOffset = new AtomicInteger(0);
//            // error signal
//            AtomicBoolean errorFlag = new AtomicBoolean(false);
//
//            log.info("subscribing to file parts");
//            // FilePart.content produces a flux of data buffers, each need to be written to the file
//            return filePart.content().doOnEach(dataBufferSignal -> {
//                if (dataBufferSignal.hasValue() && !errorFlag.get()) {
//                    // read data from the incoming data buffer into a file array
//                    DataBuffer dataBuffer = dataBufferSignal.get();
//                    int count = dataBuffer.readableByteCount();
//                    byte[] bytes = new byte[count];
//                    dataBuffer.read(bytes);
//
//                    // create a file channel compatible byte buffer
//                    final ByteBuffer byteBuffer = ByteBuffer.allocate(count);
//                    byteBuffer.put(bytes);
//                    byteBuffer.flip();
//
//                    // get the current write offset and increment by the buffer size
//                    final int filePartOffset = fileWriteOffset.getAndAdd(count);
//                    log.info("processing file part at offset {}", filePartOffset);
//                    // write the buffer to disk
//                    closeCondition.onTaskSubmitted();
//                    fileChannel.write(byteBuffer, filePartOffset, null, new CompletionHandler<Integer, ByteBuffer>() {
//                        @Override
//                        public void completed(Integer result, ByteBuffer attachment) {
//                            // file part successfuly written to disk, clean up
//                            log.info("done saving file part {}", filePartOffset);
//                            byteBuffer.clear();
//
//                            if (closeCondition.onTaskCompleted())
//                                try {
//                                    log.info("closing after last part");
//                                    fileChannel.close();
//                                } catch (IOException ignored) {
//                                    ignored.printStackTrace();
//                                }
//                        }
//
//                        @Override
//                        public void failed(Throwable exc, ByteBuffer attachment) {
//                            // there as an error while writing to disk, set an error flag
//                            errorFlag.set(true);
//                            log.info("error saving file part {}", filePartOffset);
//                        }
//                    });
//                }
//            }).doOnComplete(() -> {
//                // all done, close the file channel
//                log.info("done processing file parts");
//                if (closeCondition.canCloseOnComplete())
//                    try {
//                        log.info("closing after complete");
//                        fileChannel.close();
//                    } catch (IOException ignored) {
//                    }
//
//            }).doOnError(t -> {
//                // ooops there was an error
//                log.info("error processing file parts");
//                try {
//                    fileChannel.close();
//                } catch (IOException ignored) {
//                }
//                // take last, map to a status string
//            }).last().map(dataBuffer -> filePart.filename() + " " + (errorFlag.get() ? "error" : "uploaded"));
//        } catch (IOException e) {
//            // unable to open the file channel, return an error
//            log.info("error opening the file channel");
//            return Mono.error(e);
//        }
//    }

}
