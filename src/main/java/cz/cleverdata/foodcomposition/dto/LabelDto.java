package cz.cleverdata.foodcomposition.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import cz.cleverdata.foodcomposition.utils.dto.ImageInfoDPI;
import lombok.Builder;
import lombok.Data;

@Data
@JsonDeserialize(builder = LabelDto.LabelBuilder.class)
@Builder(builderClassName = "LabelBuilder", toBuilder = true)
public class LabelDto {

    String text;

    ImageInfoDPI imageInfoDPI;


    @JsonPOJOBuilder(withPrefix = "")
    public static class LabelBuilder {
    }

}
