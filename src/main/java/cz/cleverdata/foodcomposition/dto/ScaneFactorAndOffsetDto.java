package cz.cleverdata.foodcomposition.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

@Data
@JsonDeserialize(builder = ScaneFactorAndOffsetDto.ScaneFactorAndOffsetBuilder.class)
@Builder(builderClassName = "ScaneFactorAndOffsetBuilder", toBuilder = true)
public class ScaneFactorAndOffsetDto {

    Float scaleFactor;
    Float offset;

    @JsonPOJOBuilder(withPrefix = "")
    public static class ScaneFactorAndOffsetBuilder {
    }

}
