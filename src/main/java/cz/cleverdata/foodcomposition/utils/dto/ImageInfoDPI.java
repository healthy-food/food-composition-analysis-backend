package cz.cleverdata.foodcomposition.utils.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Data
@Builder
public class ImageInfoDPI {
    Integer physicalWidthDpi;
    Integer physicalHeightDpi;

}
