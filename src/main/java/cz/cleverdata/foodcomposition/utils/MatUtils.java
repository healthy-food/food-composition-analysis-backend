package cz.cleverdata.foodcomposition.utils;




import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.leptonica.global.lept;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.opencv_core.Mat;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Conversion between Java and OpenCV and Leptonica data types
 */
public class MatUtils {

    /**
     * @param image
     * @return
     * @throws IOException
     */
    public static Mat BufferedImage2Mat(BufferedImage image) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", byteArrayOutputStream);
        byteArrayOutputStream.flush();
        return opencv_imgcodecs.imdecode(new Mat(byteArrayOutputStream.toByteArray()), opencv_imgcodecs.IMREAD_UNCHANGED);
    }

    /**
     * @param matrix
     * @return
     * @throws IOException
     */
    public static BufferedImage Mat2BufferedImage(Mat matrix) throws IOException {
        BytePointer mob = new BytePointer();
        opencv_imgcodecs.imencode(".jpg", matrix, mob);
        return ImageIO.read(new ByteArrayInputStream(mob.getStringBytes()));
    }

    public static PIX convertMatToPix(Mat mat) {
        BytePointer bytes = new BytePointer();
        opencv_imgcodecs.imencode(".tif", mat, bytes);
        ByteBuffer buff = ByteBuffer.wrap(bytes.getStringBytes());
        return lept.pixReadMem(buff, buff.capacity());
    }

//    public static Mat convertPixToMat(PIX pix) {
//        PointerByReference pdata = new PointerByReference();
//        NativeSizeByReference psize = new NativeSizeByReference();
//        Leptonica1.pixWriteMem(pdata, psize, pix, ILeptonica.IFF_TIFF);
//        byte[] b = pdata.getValue().getByteArray(0, psize.getValue().intValue());
//        Leptonica1.lept_free(pdata.getValue());
//        return opencv_imgcodecs.imdecode(new Mat(b), opencv_imgcodecs.IMREAD_UNCHANGED);
//    }
}
