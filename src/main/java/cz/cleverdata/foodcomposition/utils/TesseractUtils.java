package cz.cleverdata.foodcomposition.utils;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.leptonica.PIX;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.tesseract.TessBaseAPI;
import org.bytedeco.tesseract.global.tesseract;

import java.io.IOException;

import static org.bytedeco.leptonica.global.lept.pixDestroy;

/**
 * Working with teseract utils
 *
 * www.programcreek.com/java-api-examples/?code=BlackCraze%2FGameResourceBot%2FGameResourceBot-master%2Fsrc%2Fmain%2Fjava%2Fde%2Fblackcraze%2Fgrb%2Focr%2FOCR.java#
 */
public class TesseractUtils {


    public static String readBytedecoText(Mat sourceImage) throws IOException {
        BytePointer outText;

        TessBaseAPI api = new TessBaseAPI();
        if (api.Init("src/main/resources/tessdata/best", "ces+slk+pol+deu+eng") != 0) {
            throw new RuntimeException("Could not initialize tesseract.");

        }
        api.SetPageSegMode(tesseract.PSM_AUTO_OSD);

        // Open input image with leptonica library
        PIX image = MatUtils.convertMatToPix(sourceImage);
//        PIX image = pixRead(args.length > 0 ? args[0] : "/usr/src/tesseract/testing/phototest.tif");
        api.SetImage(image);
        // Get OCR result
        outText = api.GetUTF8Text();
//        System.out.println("OCR output:\n" + outText.getString());

        // Destroy used object and release memory
        api.End();
        outText.deallocate();
        pixDestroy(image);
        return outText.getString();
    }


//    /**
//     * Extracts text from image. WIP
//     *
//     * @param bi
//     * @return
//     * @throws IOException
//     * @throws TesseractException
//     */
//    public static String readText(BufferedImage bi) throws IOException, TesseractException {
//
//        ITesseract tesseract = new Tesseract1();
//
//        tesseract.setDatapath("/usr/share/tesseract-ocr/5/tessdata");
//        tesseract.setLanguage("ces");
////        https:
////stackoverflow.com/questions/19268648/using-tesseract-to-recognize-license-plates/19418347#19418347
////            tesseract.setTessVariable("tessedit_char_whitelist", "BCDFGHJKLMNPQRSTVWXYZ0123456789-");
//        tesseract.setTessVariable("language_model_penalty_non_freq_dict_word", "1");
//        tesseract.setTessVariable("language_model_penalty_non_dict_word ", "1");
//        tesseract.setTessVariable("load_system_dawg", "0");
////            tesseract.setOcrEngineMode(ITessAPI.TessOcrEngineMode.OEM_DEFAULT);
////            tesseract.setHocr(true);
////            tesseract.setTessVariable("psm","1");
////tesseract.setConfigs();
//        return tesseract.doOCR(bi);
//    }


//    public static ScaneFactorAndOffsetDto getScaleFactorAndOffset(BufferedImage rotatedImage) {
//
//        // getting RGB content of the whole image file
//        double rgb
//                = rotatedImage
//                .getRGB(rotatedImage.getTileWidth() / 2,
//                        rotatedImage.getTileHeight() / 2);
//
//
//        Float scaleFactor = null;
//        Float offset = null;
//
//        // comparing the values
//        // and setting new scaling values
//        // that are later on used by RescaleOP
//        if (rgb >= -1.4211511E7 && rgb < -7254228) {
//            scaleFactor = 3f;
//            offset = -10f;
//        } else if (rgb >= -7254228 && rgb < -2171170) {
//            scaleFactor = 1.455f;
//            offset = -47f;
//        } else if (rgb >= -2171170 && rgb < -1907998) {
//            scaleFactor = 1.35f;
//            offset = -10f;
//        } else if (rgb >= -1907998 && rgb < -257) {
//            scaleFactor = 1.19f;
//            offset = 0.5f;
//        } else if (rgb >= -257 && rgb < -1) {
//            scaleFactor = 1f;
//            offset = 0.5f;
//        } else if (rgb >= -1 && rgb < 2) {
//            scaleFactor = 1f;
//            offset = 0.35f;
//        }
//
//        return ScaneFactorAndOffsetDto.builder().offset(offset).scaleFactor(scaleFactor).build();
//    }


}
