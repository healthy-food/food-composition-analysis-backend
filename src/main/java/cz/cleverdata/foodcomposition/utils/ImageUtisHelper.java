package cz.cleverdata.foodcomposition.utils;

import cz.cleverdata.foodcomposition.utils.dto.ImageInfoDPI;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.Imaging;
import org.bytedeco.opencv.opencv_core.Mat;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Helper to Image Utils
 */
@Slf4j
public class ImageUtisHelper {

    /**
     * WARNING - it makes things worse not better!!!!! Do no use until logical explanation Gets x,y DPI. -1 if none or cannot calculate
     *
     * @param fileName
     * @param fis
     * @return
     */
    public static ImageInfoDPI getImageInfo(FileInputStream fis,String fileName) {
        try {
            /**
             * Not sure how much this class is acurate
             */
//            ImageInfoExtractor imageInfoExtractor = new ImageInfoExtractor();
//            imageInfoExtractor.setDetermineImageNumber(true);
//            imageInfoExtractor.setInput(fis);
//            imageInfoExtractor.setCollectComments(true);
//            if (imageInfoExtractor.check()) {
////                print(sourceName, imageInfoExtractor, verbose);
//                return ImageInfoDPI.builder().physicalHeightDpi(imageInfoExtractor.getPhysicalHeightDpi()).physicalWidthDpi(imageInfoExtractor.getPhysicalHeightDpi()).build();
//            }

            final ImageInfo imageInfo = Imaging.getImageInfo(fis,fileName);
            return ImageInfoDPI.builder().physicalHeightDpi(imageInfo.getPhysicalHeightDpi()).physicalWidthDpi(imageInfo.getPhysicalWidthDpi()).build();

        } catch (Exception e) {
            log.error("Error getting Exif metadata", e);
        }
        return null;
    }

    public static Mat resize(Mat sourcePicture, Integer width, Integer height) throws IOException {
        BufferedImage bufferedImage = MatUtils.Mat2BufferedImage(sourcePicture);
        return MatUtils.BufferedImage2Mat(Thumbnails.of(bufferedImage)
                .height(height)
                .width(width)
                .asBufferedImage());
    }

}
