package cz.cleverdata.foodcomposition.utils;


import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.stereotype.Service;

import static org.bytedeco.opencv.global.opencv_imgproc.*;

/**
 * Utils which are at least particulary tested. from OpenCV and other directions
 */
@Service
public class OpenCVAndImageStableUtils {

    static final double MINIMUM_DESKEW_THRESHOLD = 0.05d;

    @Autowired
    private Environment environment;

    /**
     * Colored photo do grayed photo
     *
     * @param sourceImage
     * @param fileName
     * @return
     */
    public Mat colorToGray(Mat sourceImage, String fileName) {

        Mat imgGray = new Mat();

        Boolean isTestProfileActive = environment.acceptsProfiles(Profiles.of("test", "debug"));

        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("target/"+fileName + "_colorToGray" + "_Input.png", sourceImage);
        }


        opencv_imgproc.cvtColor(sourceImage, imgGray, opencv_imgproc.COLOR_BGR2GRAY);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("target/"+fileName + "_colorToGray" + "_Image.png", imgGray);
        }

        return imgGray;
    }


    /**
     * Make image more smooth by blurring, reduce noise
     *
     * @param sourceImage
     * @param fileName
     * @return
     */
    public Mat gaussianBlurNoiseRemoval(Mat sourceImage, String fileName) {

        Mat imgGaussianBlur = new Mat();

        Boolean isTestProfileActive = environment.acceptsProfiles(Profiles.of("test", "debug"));

        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("target/"+fileName + "_gaussianBlurNoiseRemoval" + "_Input.png", sourceImage);
        }


        opencv_imgproc.GaussianBlur(sourceImage, imgGaussianBlur, new Size(3, 3), 0);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("target/"+fileName + "_gaussianBlurNoiseRemoval" + "_Image.png", imgGaussianBlur);
        }

        return imgGaussianBlur;
    }

    /**
     * Image to black and white
     *
     * @param sourceImage
     * @param fileName
     * @return
     */
    public Mat toBlackAndWhite(Mat sourceImage, String fileName) {

        Mat imgThreshold = new Mat();

        Boolean isTestProfileActive = environment.acceptsProfiles(Profiles.of("test", "debug"));

        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("target/"+fileName + "toBlackAndWhite" + "_Input.png", sourceImage);
        }

//127,255
        opencv_imgproc.threshold(sourceImage, imgThreshold, 0, 255, CV_THRESH_OTSU + CV_THRESH_BINARY);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("target/"+fileName + "toBlackAndWhiteThreshold" + "_Image.png", imgThreshold);
        }

        return imgThreshold;
    }

//    /**
//     * Removes alpha channel
//     *
//     * @param sourceImage
//     * @param fileName
//     * @return
//     */
//    public Mat removeAlphaChannel(Mat sourceImage, String fileName) {
//
//        Mat imgRemoveAlphaChannel = new Mat();
//
//        Boolean isTestProfileActive = environment.acceptsProfiles(Profiles.of("test", "debug"));
//
//        if (isTestProfileActive) {
//            opencv_imgcodecs.imwrite("target/"+fileName+"_removeAlphaChannel"+"_Input.png", sourceImage);
//        }
//
//        opencv_imgproc.(sourceImage, imgRemoveAlphaChannel, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY_INV, 75, 35);
//        if (isTestProfileActive) {
//            opencv_imgcodecs.imwrite("target/"+fileName+"_removeAlphaChannel"+"_Image.png", sourceImage);
//        }
//
//        return imgRemoveAlphaChannel;
//    }


    /**
     * Image to black and white
     *
     * @param sourceImage
     * @param fileName
     * @return
     */
    public Mat toBlackAndWhiteAdaptive(Mat sourceImage, String fileName) {

        Mat imgAdaptiveThreshold = new Mat();

        Boolean isTestProfileActive = environment.acceptsProfiles(Profiles.of("test", "debug"));

        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("target/"+fileName + "_toBlackAndWhiteAdaptive" + "_Input.png", sourceImage);
        }

        opencv_imgproc.adaptiveThreshold(sourceImage, imgAdaptiveThreshold, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 11, 2);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("target/"+fileName + "_toBlackAndWhiteAdaptiveThreshold" + "_Image.png", sourceImage);
        }

        return imgAdaptiveThreshold;
    }

//    /**
//     * Auto rotates image
//     *
//     * @param mat
//     * @return
//     * @throws IOException
//     */
//    public Mat autoRotate(Mat mat) throws IOException {
//        BufferedImage bi = MatUtils.Mat2BufferedImage(mat);
//        ImageDeskew id = new ImageDeskew(bi);
//        double imageSkewAngle = id.getSkewAngle(); // determine skew angle
//        if ((imageSkewAngle > MINIMUM_DESKEW_THRESHOLD || imageSkewAngle < -(MINIMUM_DESKEW_THRESHOLD))) {
//            bi = ImageHelper.rotateImage(bi, -imageSkewAngle); // deskew image
//        }
//        return MatUtils.BufferedImage2Mat(bi);
//    }


}
