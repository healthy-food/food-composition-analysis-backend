package cz.cleverdata.foodcomposition.experimental.dto;

import lombok.Builder;
import lombok.Data;
import org.bytedeco.opencv.opencv_core.Mat;

@Builder
@Data
public class RemoveShadowsDto {

    Mat result;

    Mat resultNorm;
}
