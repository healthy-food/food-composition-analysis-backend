package cz.cleverdata.foodcomposition.experimental;

//http://stackoverflow.com/questions/37302098/image-preprocessing-with-opencv-before-doing-character-recognition-tesseract/


import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.*;

import static org.bytedeco.opencv.global.opencv_imgproc.MORPH_RECT;
import static org.bytedeco.opencv.global.opencv_imgproc.getStructuringElement;


/**
 * This is sandbox. Experiment here
 */
public class OpenCVExperimetalUtils {

    private static final int
            CV_MOP_CLOSE = 3,
            CV_THRESH_OTSU = 8,
            CV_THRESH_BINARY = 0,
            CV_ADAPTIVE_THRESH_GAUSSIAN_C = 1,
            CV_ADAPTIVE_THRESH_MEAN_C = 0,
            CV_THRESH_BINARY_INV = 1;


    private static boolean checkRatio(RotatedRect candidate) {
        double error = 0.3;
        //Spain car plate size: 52x11 aspect 4,7272
        //Russian 12x2 (52cm / 11.5)
        //double aspect = 52/11.5;
        double aspect = 6;
        int min = 15 * (int) aspect * 15;
        int max = 125 * (int) aspect * 125;
        //Get only patchs that match to a respect ratio.
        double rmin = aspect - aspect * error;
        double rmax = aspect + aspect * error;
        double area = candidate.size().height() * candidate.size().width();
        float r = (float) candidate.size().width() / (float) candidate.size().height();
        if (r < 1)
            r = 1 / r;
        if ((area < min || area > max) || (r < rmin || r > rmax)) {
            return false;
        } else {
            return true;
        }
    }

    private static boolean checkDensity(Mat candidate) {
        float whitePx = 0;
        float allPx = 0;
        whitePx = opencv_core.countNonZero(candidate);
        allPx = candidate.cols() * candidate.rows();
        //System.out.println(whitePx/allPx);
        if (0.62 <= whitePx / allPx)
            return true;
        else
            return false;
    }

    /**
     * Do not use it, this is just for testing purposes
     * @param inputImage
     * @param isTestProfileActive
     * @return
     * @throws InterruptedException
     */
    public static Mat processImageByOpenCV(Mat inputImage, Boolean isTestProfileActive) throws InterruptedException {


        Mat imgGray = new Mat();
        Mat imgGaussianBlur = new Mat();
        Mat imgSobel = new Mat();
        Mat imgThreshold = new Mat();
        Mat imgAdaptiveThreshold = new Mat();
        Mat imgAdaptiveThreshold_forCrop = new Mat();
        Mat imgMoprhological = new Mat();
        Mat imgContours = new Mat();
        Mat imgMinAreaRect = new Mat();
        Mat imgDetectedPlateCandidate = new Mat();
        Mat imgDetectedPlateTrue = new Mat();

        MatVector contours = new MatVector();

        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("1_True_Image.png", inputImage);
        }


        opencv_imgproc.cvtColor(inputImage, imgGray, opencv_imgproc.COLOR_BGR2GRAY);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("2_imgGray.png", imgGray);
        }


        opencv_imgproc.GaussianBlur(imgGray, imgGaussianBlur, new Size(3, 3), 0);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("3_imgGaussianBlur.png", imgGaussianBlur);
        }

        opencv_imgproc.Sobel(imgGaussianBlur, imgSobel, -1, 1, 0);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("4_imgSobel.png", imgSobel);
        }

        opencv_imgproc.threshold(imgSobel, imgThreshold, 0, 255, CV_THRESH_OTSU + CV_THRESH_BINARY);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("5_imgThreshold.png", imgThreshold);
        }

        opencv_imgproc.adaptiveThreshold(imgSobel, imgAdaptiveThreshold, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY_INV, 75, 35);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("5_imgAdaptiveThreshold.png", imgAdaptiveThreshold);
        }

        opencv_imgproc.adaptiveThreshold(imgGaussianBlur, imgAdaptiveThreshold_forCrop, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 99, 4);
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("5_imgAdaptiveThreshold_forCrop.png", imgAdaptiveThreshold_forCrop);
        }

        Mat element = getStructuringElement(MORPH_RECT, new Size(17, 3));
        opencv_imgproc.morphologyEx(imgAdaptiveThreshold, imgMoprhological, CV_MOP_CLOSE, element); //или imgThreshold
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("6_imgMoprhologicald.png", imgMoprhological);
        }

        imgContours = imgMoprhological.clone();
        opencv_imgproc.findContours(imgContours,
                contours,
                new Mat(),
                opencv_imgproc.RETR_LIST,
                opencv_imgproc.CHAIN_APPROX_SIMPLE);

        opencv_imgproc.drawContours(imgContours, contours, -1, new Scalar(255, 0, 0, 0));
        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("7_imgContours.png", imgContours);
        }

        imgMinAreaRect = inputImage.clone();
        Mat[] matOfPoint = contours.get();
        for (Mat gpuMat : matOfPoint) {

            RotatedRect box = opencv_imgproc.minAreaRect(gpuMat);
            if (checkRatio(box)) {
                opencv_imgproc.rectangle(imgMinAreaRect, box.boundingRect().tl(), box.boundingRect().br(), new Scalar(0, 0, 255, 0));
                imgDetectedPlateCandidate = new Mat(imgAdaptiveThreshold_forCrop, box.boundingRect());
                if (checkDensity(imgDetectedPlateCandidate))
                    imgDetectedPlateTrue = imgDetectedPlateCandidate.clone();
            } else
                opencv_imgproc.rectangle(imgMinAreaRect, box.boundingRect().tl(), box.boundingRect().br(), new Scalar(0, 255, 0, 0));

        }

        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite("8_imgMinAreaRect.png", imgMinAreaRect);
            if (!imgDetectedPlateCandidate.empty())
                opencv_imgcodecs.imwrite("9_imgDetectedPlateCandidate.png", imgDetectedPlateCandidate);
            if (!imgDetectedPlateTrue.empty())
                opencv_imgcodecs.imwrite("10_imgDetectedPlateTrue.png", imgDetectedPlateTrue);
        }
        return imgDetectedPlateTrue;
    }
}