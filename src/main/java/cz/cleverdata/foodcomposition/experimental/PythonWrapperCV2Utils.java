package cz.cleverdata.foodcomposition.experimental;


import cz.cleverdata.foodcomposition.experimental.dto.RemoveShadowsDto;
import org.bytedeco.opencv.global.opencv_core;
import org.bytedeco.opencv.global.opencv_imgcodecs;
import org.bytedeco.opencv.global.opencv_imgproc;
import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.MatVector;

/**
 * Uses python's libraries
 */
public class PythonWrapperCV2Utils {
    /**
     * https://stackoverflow.com/questions/44752240/how-to-remove-shadow-from-scanned-images-using-opencv
     */
    public static RemoveShadowsDto removeShadows(Mat sourceImage, String fileName, Boolean isTestProfileActive) {

        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite(fileName+"removeShadows"+"_Input.png", sourceImage);
        }




        MatVector rgbPlanes = new MatVector(), resultPlanes = new MatVector(), resultNormPlanes = new MatVector();
        opencv_core.split(sourceImage, rgbPlanes);
        Mat rgbPlane = new Mat(), dilatedImage = new Mat(), medianBlurImage = new Mat(), diffImage = new Mat(), normImage = new Mat();
        Mat result = new Mat(), resultNorm = new Mat();

        while (!rgbPlanes.empty()) {
            rgbPlane = rgbPlanes.pop_back();
            opencv_imgcodecs.imwrite(fileName+"removeShadows_plane"+"_Input.png", sourceImage);
            opencv_imgproc.dilate(rgbPlane, dilatedImage, Mat.ones(7, 7, opencv_core.CV_8U).asMat());
            opencv_imgproc.medianBlur(dilatedImage, medianBlurImage, 21);
            opencv_core.absdiff(rgbPlane, medianBlurImage, diffImage);
            opencv_core.normalize(diffImage, normImage, 0d, 255d, opencv_core.NORM_MINMAX, opencv_core.CV_8UC1, null); //posledni parametr?
            resultPlanes = resultPlanes.put(diffImage);
            resultNormPlanes = resultNormPlanes.put(normImage);
        }

        opencv_core.merge(resultPlanes, result);
        opencv_core.merge(resultNormPlanes, resultNorm);


        if (isTestProfileActive) {
            opencv_imgcodecs.imwrite(fileName+"removeShadows"+"_Image.png", resultNorm);
        }

        return RemoveShadowsDto.builder().result(result).resultNorm(resultNorm).build();
    }
}
