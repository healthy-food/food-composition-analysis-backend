//package cz.cleverdata.foodcomposition.experimental;
//
//
//import org.bytedeco.javacpp.BytePointer;
//import org.bytedeco.leptonica.PIX;
//import org.bytedeco.opencv.opencv_core.Mat;
//import org.bytedeco.tesseract.TessBaseAPI;
//
//import java.awt.image.BufferedImage;
//import java.io.IOException;
//
//import static org.bytedeco.leptonica.global.lept.pixDestroy;
//import static org.bytedeco.leptonica.global.lept.pixRead;
//
///**
// * Working with teseract utils
// */
//public class TesseractBytedecoUtils {
//
//
//
//
//    /**
//     * Extracts text from image. WIP
//     *
//     * @param sourceImage
//     * @return
//     * @throws IOException
//     */
//    public static String readText(Mat sourceImage) throws IOException {
//
//        BytePointer outText;
//
//        TessBaseAPI api = new TessBaseAPI();
//        // Initialize tesseract-ocr with English, without specifying tessdata path
//        if (api.Init(null, "eng") != 0) {
//            System.err.println("Could not initialize tesseract.");
//            System.exit(1);
//        }
//
//        // Open input leptonicaImage with leptonica library
//        PIX leptonicaImage = pixRead(sourceImage.data());
//        api.SetImage(leptonicaImage);
//        // Get OCR result
//        outText = api.GetUTF8Text();
////        System.out.println("OCR output:\n" + outText.getString());
//
//        // Destroy used object and release memory
//        api.End();
//        outText.deallocate();
//        pixDestroy(leptonicaImage);
//        return outText.getString();
//
//    }
//
//
//
//
//
//
//
//}
