package cz.cleverdata.foodcomposition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Component
public class FoodcompositionTestApplication {

    public static void main(String[] args) {
//        SpringApplication.run(FoodcompositionTestApplication.class, args);

        SpringApplication springApplication=new SpringApplication(FoodcompositionTestApplication.class);
        String[] profiles= new String[]{"test","test-extra"}; // array of profiles, create array of one profile in case single profile
        springApplication.setAdditionalProfiles(profiles); // set active profile, We can write condition here based on requirements
        springApplication.run(args);  // run spring boot application
    }

}