package cz.cleverdata.foodcomposition.cucumber;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cz.cleverdata.foodcomposition.FoodcompositionTestApplication;
import cz.cleverdata.foodcomposition.ahocorasick.AhocorasickUtils;
import cz.cleverdata.foodcomposition.dto.LabelDto;
import io.cucumber.java.en.And;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.time.Duration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FoodcompositionTestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@ActiveProfiles("test")
@AutoConfigureWebTestClient
@AutoConfigureMockMvc
public class LabelStepDefs {

    LabelDto labelDto;

    //    @Autowired
//    JdbcTemplate jdbcTemplate;
//
//    //Intellij Idea bug
//    @SuppressWarnings(value = "")
//    @Autowired
//    private DataSource dataSource;
    private ObjectMapper json;
    private String uuid;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @PostConstruct
    public void initMapper() {
        // Register an adapter to manage the date types as long values and vice versa
        json = new ObjectMapper().registerModules(new Jdk8Module(), new JavaTimeModule())
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        webTestClient = webTestClient
                .mutate()
                .responseTimeout(Duration.ofMillis(300000))
                .build();
    }

//    private void clearDatabase() throws SQLException {
//        Connection c = dataSource.getConnection();
//        Statement s = c.createStatement();
//
//        // Disable FK
//        s.execute("SET REFERENTIAL_INTEGRITY FALSE");
//
//        // Find all tables and truncate them
//        Set<String> tables = new HashSet<String>();
//        ResultSet rs = s.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA='PUBLIC'");
//        while (rs.next()) {
//            tables.add(rs.getString(1));
//        }
//        rs.close();
//        for (String table : tables) {
//            s.executeUpdate("TRUNCATE TABLE " + table);
//        }
//
//        // Idem for sequences
//        Set<String> sequences = new HashSet<String>();
//        rs = s.executeQuery("SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='PUBLIC'");
//        while (rs.next()) {
//            sequences.add(rs.getString(1));
//        }
//        rs.close();
//        for (String seq : sequences) {
//            s.executeUpdate("ALTER SEQUENCE " + seq + " RESTART WITH 1");
//        }
//
//        // Enable FK
//        s.execute("SET REFERENTIAL_INTEGRITY TRUE");
//        s.close();
//    }

    @Given("^Clean database$")
    public void vycistitDatabaziUzivatelskehoRozhrani() throws SQLException {
//        this.clearDatabase();
        log.info("Vycistit zaznam");
    }


    @Then("Expecting (.*) contains at least (.*)$")
    public void expectingAtLeastRecognizedText(String imageFile, String shouldContain) {
        String recognizedText = labelDto.getText();
        log.info("Ending processing of file = "+imageFile);
        Assert.assertTrue("Response from server does not contain required words = " + shouldContain + " for image = " + imageFile, AhocorasickUtils.containsWordsAhoCorasick(recognizedText, shouldContain));
    }

    //    @When("Upload sync file (.*) in language (.*) and expect status (\\d+)$")
    @When("Upload sync file (.*) in language (.*) and expect status (\\d+)$")
    public void uploadSyncFileImageFileInLanguageLanguageAndExpectStatusStatus(String imageFile, String language, int status) throws Exception {
        final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("labels/" + language + "/" + imageFile);
        final MockMultipartFile avatar = new MockMultipartFile("file", "test.png", "image/png", inputStream);


        final MvcResult result = mockMvc.perform(fileUpload("/api/uploadLabel/sync").file(avatar).requestAttr("imageName", imageFile))
                .andExpect(status().isOk())
                .andReturn();

        labelDto = json.readValue(result.getResponse().getContentAsString(Charset.forName("UTF-8")), LabelDto.class);


//        webTestClient.post().uri("/api/uploadLabel/").body(Mono.just(palletDto), File.class).exchange().expectStatus().isEqualTo(status).returnResult(PalletDto.class).getResponseBodyContent()

    }


    @And("Actual dpi is right for tesseract")
    public void actualDpiIsRightForTesseract() {
        if(labelDto.getImageInfoDPI() != null) {
            log.debug("Width is " + labelDto.getImageInfoDPI().getPhysicalWidthDpi() + " and Heigth is " + labelDto.getImageInfoDPI().getPhysicalHeightDpi());
            if (labelDto.getImageInfoDPI().getPhysicalWidthDpi() != -1 && labelDto.getImageInfoDPI().getPhysicalHeightDpi() != -1) {
                Assert.assertTrue("Height is less than 300 and is =" + labelDto.getImageInfoDPI().getPhysicalHeightDpi(), labelDto.getImageInfoDPI().getPhysicalHeightDpi() >= 300);
                Assert.assertTrue("Width is less than 300 and is =" + labelDto.getImageInfoDPI().getPhysicalWidthDpi(), labelDto.getImageInfoDPI().getPhysicalWidthDpi() >= 300);
            }
        }
    }

//    @io.cucumber.java.en.Then("Expecting (.*) contains at least <shouldContain>")
//    public void expectingImageFileCantainsAtLeastShouldContain() {
//    }
}
