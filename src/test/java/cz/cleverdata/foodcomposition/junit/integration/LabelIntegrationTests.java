package cz.cleverdata.foodcomposition.junit.integration;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Profile;


@RunWith(CucumberWithSerenity.class)
//@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features/labelIntegrationTests.feature"}, glue = "cz.cleverdata.foodcomposition.cucumber")
@SuppressWarnings({"hideutilityclassconstructor", "squid:s2187"})
public class LabelIntegrationTests {

}
