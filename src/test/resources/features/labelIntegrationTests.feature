Feature: Operations with label


  #Now contains only several pictures After gathering 100 do much more tests
  Scenario Outline: Read text from various labels
#    Given Clean database
    When Upload sync file <imageFile> in language <language> and expect status <status>
    Then Expecting <imageFile> contains at least <shouldContain>
    And Actual dpi is right for tesseract

    Examples:
      | imageFile                                | status | language | shouldContain       |
      | mleko.jpg                                | 200    | cz       | UHT,mléko           |
      | horcice.jpeg                             | 200    | cz       | Hořčice,ocet        |
      | mleko_bio.jpeg                           | 200    | cz       | Bio,ekologického    |
      | olivy_svetlo.jpeg                        | 200    | cz       | OLIVY               |
      | olivy.jpeg                               | 200    | cz       | OLIVY               |
      | parenica.jpeg                            | 200    | cz       | sůl,mléko           |
      | crema_caffe_smetanovy_liker.jpg          | 200    | cz       | uhličitan,sodný     |
      | queen_margot_smetanovy_liker.jpg         | 200    | cz       | whisky              |
      | univerzalni_sypky_korenici_pripravek.jpg | 200    | cz       | dextróza,mléka      |
      | cokolada_bombony.jpg                     | 200    | cz       | kokos               |
      | cokolada_mandle.jpg                      | 200    | cz       | cukr                |
      | cokolada_mlecna.jpg                      | 200    | cz       | palmový olej        |
      | cokolada_smes.jpg                        | 200    | cz       | čokoláda            |
      | cokolady_vybrane.jpg                     | 200    | cz       | mandle              |
      | draze_z_mlecne_cokolady.jpg              | 200    | cz       | kakaová hmota       |
      | horcice.jpg                              | 200    | cz       | ocet                |
      | horcice_plnotucna.jpg                    | 200    | cz       | kurkuma             |
      | instantni_kakaovy_napoj_s_vitaminy.jpg   | 200    | cz       | dextróza            |
      | kakaovy_napoj.jpg                        | 200    | cz       | škrob               |
      | kavovy_instantni_napoj_bellaram.jpg      | 200    | cz       | lepek               |
      | kavovy_krem.jpg                          | 200    | cz       | řepkový olej        |
      | kynute_knedliky_s_povidly.jpg            | 200    | cz       | švestkový lektvar   |
      | lovecky_salam.jpg                        | 200    | cz       | jedlá sůl           |
      | marcipanova_stola.jpg                    | 200    | cz       | citrónová kůra      |
      | margarin_light.jpg                       | 200    | cz       | palmový tuk         |
      | mlecna_cokolada.jpg                      | 200    | cz       | glukózový sirup     |
      | oplatka_s_kremovou_naplni.jpg            | 200    | cz       | bramborový škrob    |
      | plody_v_cokolade.jpg                     | 200    | cz       | šelak               |
      | polican.jpg                              | 200    | cz       | dusitan sodný       |
      | prazska_sunka.jpg                        | 200    | cz       | trifosforečnany     |
      | rozinky_sezamove.jpg                     | 200    | cz       | lecitiny            |
      | sezamova_tycinka.jpg                     | 200    | cz       | glukózový sirup     |
      | sunka_shaved.jpg                         | 200    | cz       | E450,E451,E452,E310 |
      | sunkovy_salam.jpg                        | 200    | cz       | dusitan sodný       |
      | syr.jpg                                  | 200    | cz       | syřidlo             |
      | syr_dansky.jpg                           | 200    | cz       | sýrařské kultury    |
      | syr_niva.jpg                             | 200    | cz       | mléko               |
      | tunakovy_salat.jpg                       | 200    | cz       | kukuřičný olej      |
      | tvarohovy_desert_vanilka.jpg             | 200    | cz       | dusík               |
      | uzena_krkovice.jpg                       | 200    | cz       | erythorban sodný    |
      | uzena_veprova_kyta.jpg                   | 200    | cz       | askorban sodný      |
      | uzeny_bucek.jpg                          | 200    | cz       | glukóza             |
      | vanilkovy_jogurtovy_desert.jpg           | 200    | cz       | glukózový sirup     |
      | zloutkove_susenky.jpg                    | 200    | cz       | palmový tuk         |
