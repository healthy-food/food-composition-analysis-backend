### BUILD image

FROM maven:3-jdk-11 as builder

# create app folder for sources

RUN mkdir -p /build

WORKDIR /build

COPY pom.xml /build

#Download all required dependencies into one layer

RUN mvn -B dependency:resolve dependency:resolve-plugins

#Copy source code

COPY src /build/src

# Build application only fox linux-x86_64 platform

RUN mvn -Djavacpp.platform.custom -Djavacpp.platform.host -Djavacpp.platform.linux-x86_64 -DskipTests clean package

FROM adoptopenjdk/openjdk11:latest as runtime

#tesseract...to be removed
#RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository -y ppa:alex-p/tesseract-ocr
#RUN apt-get update && apt-get install -y tesseract-ocr-all libtesseract-dev tesseract-ocr
#Set app home folder
ENV APP_HOME /app
#Possibility to set JVM options (https://www.oracle.com/technetwork/java/javase/tech/vmoptions-jsp-140102.html)
#ENV JAVA_OPTS="-Xmx250m -Xms250m "
#Create base app folder
RUN mkdir $APP_HOME
#Create folder to save configuration files
RUN mkdir $APP_HOME/config
#Create folder with application logs
RUN mkdir $APP_HOME/log
VOLUME $APP_HOME/log
VOLUME $APP_HOME/config
WORKDIR $APP_HOME
#Copy executable jar file from the builder image
COPY --from=builder /build/target/*.jar kfm-backend.jar
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -agentlib:jdwp=transport=dt_socket,address=*:5005,server=y,suspend=n -Djava.security.egd=file:/dev/./urandom -jar kfm-backend.jar" ]
#CMD java -Xmx300m -Xms300m -Djava.security.egd=file:/dev/./urandom -jar kfm-backend.jar -spring.profiles.active=docker
#Second option using shell form:
#ENTRYPOINT exec java $JAVA_OPTS -jar kfm-backend.jar $0 $@
EXPOSE 8080
EXPOSE 5005
