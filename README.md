# food-composition-analysis 


This project aim to analyse food from stores by their label.

Typical usage is to take picture of food composition we would lke to analyse and from text additives would be recognized alongside with other possible harming food admixtures. Also ecological impact of used additives will be analysed.


2,Installation
Later all solution will be dockerized.

For now:
a) Install tesseract
b) Install tessdata to /usr/share/tesseract-ocr/4.00/tessdata
c) Java 11
e) Opencv - JavaCv, bytedeco.  
f) Maven run    "mvn clean install"

3.Test
Currently there are test pictures with composition. Text recognition is performed and result is compared to expectation.

